# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'io/console'
require 'timeout'
require_relative 'lib/core'

module FireIsRubyCmd
  def self.play
    game = Core.get_new_playing_field
    frame_animation = false
    frame_timeout = 0.5
    game_over = false
    lines_removed = 0
    expected_timeout = Time.new + frame_timeout
    puts 'Press escape or control-c to exit.'
    loop do
      # Draw.
      system 'clear' or system 'cls'
      puts 'Press escape, q or control-c to exit. Press r to reset. %s' % (frame_animation ? '\\' : '/')
      game_cells = game.playing_field_cells
      (0...game.playing_field_height).each do |y|
        (0...game.playing_field_width).each do |x|
          if game.cell_contain_the_falling_tetriminio game_cells[x][y]
            print 'O'
          elsif game_cells[x][y].is_active
            print '#'
          else
            print '_'
          end
        end
        puts
      end
      game_details = [
          lines_removed,
          game.falling_tetriminio.name,
          game.falling_tetriminio.get_current_shape_symbol.to_s.scan(/\d+/).first,
          game.falling_tetriminio.cell_pos_x,
          game.falling_tetriminio.cell_pos_y
      ]
      puts 'Score:%d Shape:%s Direction:%s x:%d y:%d' % game_details
      if game_over
        puts "Game's over, sir or madam.. -.-'"
      end

      # Input.
      cki = ConsoleHelper.read_key_with_timeout frame_timeout

      # Action.
      exit 0 if game_over
      frame_animation = !frame_animation
      case cki
        when 'q', 'ESCAPE', 'CONTROL-C'
          exit 0
        when 'r'
          game.reset_playing_field
          game_over = false
          lines_removed = 0
        when 'x'
          game.rotate_tetriminio_90_degree_right
        when 'z', 'UP ARROW'
          game.rotate_tetriminio_90_degree_left
        when 'DOWN ARROW'
          # Force timeout of frame, see expected_timeout check further down.
          expected_timeout = Time.new
        when 'RIGHT ARROW'
          game.move_tetriminio_right
        when 'LEFT ARROW'
          game.move_tetriminio_left
        when 'SPACE'
          game.move_tetriminio_to_bottom
          lines_removed += game.try_to_remove_solid_rows
      end
      if Time.new >= expected_timeout
        game_over = !game.move_tetriminio_down
        lines_removed += game.try_to_remove_solid_rows
        expected_timeout = Time.new + frame_timeout
      end
    end
  end

  # Credit: https://gist.github.com/acook/4190379
  class ConsoleHelper

    def self.read_key_with_timeout(seconds)
      c = Timeout::timeout(seconds) do
        read_char
      end

      case c
        when ' '
          'SPACE'
        when "\t"
          'TAB'
        when "\r"
          'RETURN'
        when "\n"
          'LINE FEED'
        when "\e"
          'ESCAPE'
        when "\e[A"
          'UP ARROW'
        when "\e[B"
          'DOWN ARROW'
        when "\e[C"
          'RIGHT ARROW'
        when "\e[D"
          'LEFT ARROW'
        when "\177"
          'BACKSPACE'
        when "\004"
          'DELETE'
        when "\e[3~"
          'ALTERNATE DELETE'
        when "\u0003"
          'CONTROL-C'
        when /^.$/
          #"SINGLE CHAR HIT: #{c.inspect}"
          c
        when nil
          nil
        else
          "SOMETHING ELSE: #{c.inspect}"
      end
    end

    # Reads keypresses from the user including 2 and 3 escape character sequences.
    def self.read_char
      STDIN.echo = false
      STDIN.raw!
      input = STDIN.getc.chr
      if input == "\e" then
        input << STDIN.read_nonblock(3) rescue nil
        input << STDIN.read_nonblock(2) rescue nil
      end
    ensure
      STDIN.echo = true
      STDIN.cooked!
      return input
    end
  end # ConsoleHelper end.

end

FireIsRubyCmd.play