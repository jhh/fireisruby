# FireIsRuby - A Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'gosu'
require_relative 'lib/core'
require_relative 'lib/score'

class FireIsRubyGosu < Gosu::Window

  CELL_HEIGHT = 30
  CELL_WIDTH = 30
  WINDOW_HEIGHT = 600
  WINDOW_WIDTH = 800
  FULLSCREEN = false
  FRAME_TIMEOUT = 500.0 # Milliseconds.

  def initialize
    super WINDOW_WIDTH, WINDOW_HEIGHT, FULLSCREEN
    self.caption = 'FireIsRuby'

    @cell_cemented_tetriminio = Gosu::Image.new(self, 'data/cell_cemented_tetriminio.png', true)
    @cell_background = Gosu::Image.new(self, 'data/cell_background.png', true)
    @cell_empty = Gosu::Image.new(self, 'data/cell_empty.png', true)
    @cell_falling_tetriminio = Gosu::Image.new(self, 'data/cell_falling_tetriminio.png', true)
    @font = Gosu::Font.new(self, Gosu::default_font_name, 20)

    @game = Core.get_new_playing_field
    @game_over = false
    @lines_removed = 0
    @expected_timeout = Time.new + FRAME_TIMEOUT / 1000

    Score.load_score_list_from_disk true
  end

  def update
    if !@game_over
      @lines_removed += @game.try_to_remove_solid_rows
      current_time = Time.new
      if @button_down_timestamp && (@button_down_timestamp + FRAME_TIMEOUT / 5000) < current_time
        if button_down? Gosu::KbDown or button_down? Gosu::GpDown
          button_game_action Gosu::KbDown
        end
        if button_down? Gosu::KbLeft or button_down? Gosu::GpLeft
          button_game_action Gosu::KbLeft
        end
        if button_down? Gosu::KbRight or button_down? Gosu::GpRight
          button_game_action Gosu::KbRight
        end
        @button_down_timestamp = current_time
      end
      if current_time >= @expected_timeout
        @expected_timeout = current_time + FRAME_TIMEOUT / 1000
        @game_over = !@game.move_tetriminio_down
        if @game_over && Score.is_score_within_top_list?(@lines_removed)
          # Register score.
          # TODO: Input for register name.
          Score.register_score 'Anonymous', @lines_removed
          Score.save_score_list_to_disk
        end
      end
    end
  end

  def button_game_action(id)
    if id == Gosu::KbEscape
      close
    end
    if id == Gosu::KbR
      @game.reset_playing_field
      @game_over = false
      @lines_removed = 0
    end
    unless @game_over
      case id
        when Gosu::KbLeft, Gosu::GpLeft
          @game.move_tetriminio_left
        when Gosu::KbRight, Gosu::GpRight
          @game.move_tetriminio_right
        when Gosu::KbDown, Gosu::GpDown
          @expected_timeout = Time.new
        when Gosu::KbUp, Gosu::GpUp, Gosu::KbZ
          @game.rotate_tetriminio_90_degree_left
        when Gosu::KbX
          @game.rotate_tetriminio_90_degree_right
        when Gosu::KbSpace
          @game.move_tetriminio_to_bottom
      end
    end
  end

  def button_down(id)
    @button_down_timestamp = Time.new + 0.05
    button_game_action id
  end

  def button_up(id)
    @button_down_timestamp = nil
  end

  def draw
    game_cells = @game.playing_field_cells
    x_cell = y_cell = 0 # Game cell coordinates
    (0..WINDOW_WIDTH).step(CELL_WIDTH) do |x|
      (0..WINDOW_HEIGHT).step(CELL_HEIGHT) do |y|
        unless game_cells[x_cell].nil? || game_cells[x_cell][y_cell].nil?
          if @game.cell_contain_the_falling_tetriminio game_cells[x_cell][y_cell]
            @cell_falling_tetriminio.draw x, y, 0
          elsif game_cells[x_cell][y_cell].is_active
            @cell_cemented_tetriminio.draw x, y, 0
          else
            @cell_empty.draw x, y, 0
          end
        else
          @cell_background.draw x, y, 0
        end
        y_cell += 1
      end
      y_cell = 0
      x_cell += 1
    end
    x_text = game_cells.length * CELL_WIDTH + 10
    y_text = 10
    @font.draw("Score: #{@lines_removed}", x_text, y_text, 1, 1.0, 1.0, 0xffffff00)
    @font.draw('Press <escape> to exit, <r> to start over', x_text, (y_text += 20), 1, 1.0, 1.0, 0xffffff00)
    @font.draw('GAME OVER!', x_text, (y_text += 20), 1, 1.0, 1.0, 0xffffff00) if @game_over

    text_tmp = "Top '%i' list" % Score.get_max_size_of_score_list
    @font.draw(text_tmp, x_text, (y_text += 20), 1, 1.0, 1.0, 0xffffff00)
    Score.get_score_list.each do |item|
      text_tmp = "%s - %i (%s)" % [item[:name], item[:score], item[:time]]
      @font.draw(text_tmp, x_text, (y_text += 20), 1, 1.0, 1.0, 0xffffff00)
    end
  end
end

window = FireIsRubyGosu.new
window.show
