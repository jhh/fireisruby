# fireisruby
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'psych'

module Score

  @score_list = []
  MAX_SIZE_LIST = 10
  @score_file_full_path = File.join(Dir.home, '.FireIsRubyScore')

  def self.get_score_list
    @score_list
  end

  def self.get_max_size_of_score_list
    MAX_SIZE_LIST
  end

  def self.register_score(name, score)
    @score_list << {name: name, score: score, time: Time.new}
    @score_list.sort_by! { |item|
      item[:score]
    }.reverse!
    if @score_list.length > MAX_SIZE_LIST
      @score_list.pop(@score_list.length - MAX_SIZE_LIST)
    end
  end

  def self.is_score_within_top_list?(score)
    if @score_list.length < MAX_SIZE_LIST
      true
    else
      @score_list[MAX_SIZE_LIST - 1][:score] < score
    end
  end

  def self.yaml_score_list
    Psych.dump(Marshal.dump(@score_list))
  end

  def self.undo_yaml_score_list(yaml_string)
    unless yaml_string.instance_of? String
      throw ArgumentError, 'yaml_string parameter is not String'
    end
    obj = Marshal.load(Psych.load(yaml_string))
    unless obj.instance_of? Array
      throw ArgumentError, 'Can not un-yaml parameter, expected Array'
    end
    @score_list = obj
  end

  def self.load_score_list_from_disk(create_if_not_exists=false)
    if create_if_not_exists && !File.exist?(@score_file_full_path)
      save_score_list_to_disk
    end
    file_content = ''
    File.open(@score_file_full_path, 'r') do |f|
      file_content = f.read
    end
    self.undo_yaml_score_list file_content
  end

  def self.save_score_list_to_disk
    score = self.yaml_score_list
    File.open(@score_file_full_path, 'w') do |f|
      f.puts score
    end
  end
end