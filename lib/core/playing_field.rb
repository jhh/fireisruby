# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'providers/tetriminio'
require_relative 'providers/tetriminio_provider'

module Core
  class PlayingField

    PlayingFieldCell = Struct.new(:tetriminio, :is_active)

    attr_reader :playing_field_height
    attr_reader :playing_field_width
    attr_reader :tetriminio_default_y_position
    attr_reader :playing_field_cells
    attr_reader :falling_tetriminio

    def initialize(tetriminio_provider)
      @tetriminio_provider = tetriminio_provider
      @playing_field_height = 20
      @playing_field_width = 10
      @tetriminio_default_y_position = 0
      reset_playing_field
    end


    def reset_playing_field
      @playing_field_cells = get_new_playing_field_cells
      @falling_tetriminio = get_random_tetriminio
      put_tetriminio @falling_tetriminio, true
    end

    def put_tetriminio(falling_tetriminio, set_active=false)
      playing_field_cells_for falling_tetriminio do |cell|
        cell.is_active = set_active
        cell.tetriminio = falling_tetriminio
      end
    end

    # Check if tetriminio can be placed in playing field.
    #
    # If cell falls outside playing field, false returns.
    # If an active cell exists where tetriminio will be placed,
    # false returns.
    # Otherwise, true.
    def dry_put_tetriminio(tetriminio)
      result = true
      catch(:cell_outside_field_or_active_cell_exists) do
        playing_field_cells_for tetriminio do |cell|
          if cell.nil? || cell.is_active
            result = false
            throw(:cell_outside_field_or_active_cell_exists)
          end
        end
      end
      result
    end

    def rotate_tetriminio_90_degree_right
      # Deactivate before rotate
      put_tetriminio @falling_tetriminio, false
      @falling_tetriminio.rotate_tetriminio_90_degree_right
      # Restore position if tetriminio can't be rotated.
      unless dry_put_tetriminio @falling_tetriminio
        @falling_tetriminio.rotate_tetriminio_90_degree_left
      end
      # Reactivate.
      put_tetriminio @falling_tetriminio, true
    end

    def rotate_tetriminio_90_degree_left
      # Deactivate before rotate
      put_tetriminio @falling_tetriminio, false
      @falling_tetriminio.rotate_tetriminio_90_degree_left
      # Restore position if tetriminio can't be rotated.
      unless dry_put_tetriminio @falling_tetriminio
        @falling_tetriminio.rotate_tetriminio_90_degree_right
      end
      # Reactivate.
      put_tetriminio @falling_tetriminio, true
    end

    # Try to move tetriminio one step down, and
    # might create a new random tetriminio if current
    # tetriminio can't be moved down. If new random
    # tetriminio can't be put into playing field, the
    # game is most likely over.
    #
    # Returns true if current falling tetriminio
    # can be moved, or there is room for new tetriminio.
    # Returns false if there are no room for falling tetriminio
    # or the new tetriminio.
    def move_tetriminio_down
      # Deactivate before movement.
      put_tetriminio @falling_tetriminio, false
      @falling_tetriminio.cell_pos_y += 1
      if dry_put_tetriminio @falling_tetriminio
        put_tetriminio @falling_tetriminio, true
        return true
      end
      # Go back.
      @falling_tetriminio.cell_pos_y -= 1
      put_tetriminio @falling_tetriminio, true
      # Current tetriminio is stuck, so we make another.
      @falling_tetriminio = get_random_tetriminio
      if dry_put_tetriminio @falling_tetriminio
        put_tetriminio @falling_tetriminio
        return true
      end
      # Game over.
      false
    end

    def move_tetriminio_to_bottom
      current_tetriminio = @falling_tetriminio
      loop do
        break unless move_tetriminio_down && @falling_tetriminio.equal?(current_tetriminio)
      end
    end

    def try_to_remove_solid_rows
      number_of_rows_removed = 0
      # Don't process further unless tetriminio is on top of playing field.
      unless @falling_tetriminio.cell_pos_y == @tetriminio_default_y_position
        return number_of_rows_removed
      end
      # Look in each *row* (notice that y is outer, and x is inner loop).
      (0...@playing_field_height).each do |y|
        solid_row = true
        (0...@playing_field_width).each do |x|
          unless @playing_field_cells[x][y].is_active
            solid_row = false
            break
          end
        end
        # We found solid row!
        if solid_row
          clear_row y
          number_of_rows_removed += 1
        end
      end
      number_of_rows_removed
    end

    def move_tetriminio_right
      put_tetriminio @falling_tetriminio, false
      @falling_tetriminio.cell_pos_x += 1
      unless dry_put_tetriminio @falling_tetriminio
        @falling_tetriminio.cell_pos_x -= 1
      end
      put_tetriminio @falling_tetriminio, true
    end

    def move_tetriminio_left
      put_tetriminio @falling_tetriminio, false
      @falling_tetriminio.cell_pos_x -= 1
      unless dry_put_tetriminio @falling_tetriminio
        @falling_tetriminio.cell_pos_x += 1
      end
      put_tetriminio @falling_tetriminio, true
    end

    def get_random_tetriminio
      @tetriminio_provider.reset
      new_tetriminio = @tetriminio_provider.get_tetriminio_variants.sample
      new_tetriminio.cell_pos_x = @playing_field_width / 2 - new_tetriminio.get_shape[0].length / 2 - Random.rand(2)
      new_tetriminio.cell_pos_y = @tetriminio_default_y_position
      new_tetriminio
    end

    def cell_contain_the_falling_tetriminio(cell)
      cell.is_active && cell.tetriminio.equal?(@falling_tetriminio)
    end

    protected

    def clear_row(row_index)
      # Temporally hide falling tetriminio.
      put_tetriminio @falling_tetriminio, false
      new_cells = get_new_playing_field_cells
      (0...playing_field_height).each do |y|
        (0...playing_field_width).each do |x|
          if row_index > y
            # Move row down.
            new_cells[x][y+1] = @playing_field_cells[x][y]
          elsif row_index == y
            # Clear row, or skip.
            break
          else
            # No changes.
            new_cells[x][y] = @playing_field_cells[x][y]
          end
        end
      end
      @playing_field_cells = new_cells
      put_tetriminio @falling_tetriminio, true
    end

    def get_new_playing_field_cells
      return Array.new(playing_field_width) do
        Array.new(playing_field_height).fill do
          PlayingFieldCell.new(nil, false)
        end
      end
    end

    def playing_field_cells_for(tetriminio)
      raise TypeError, 'Expected Tetriminio' unless tetriminio.instance_of?(Providers::Tetriminio)

      x_shape_range = (0...tetriminio.get_shape.length)
      x_pos = tetriminio.cell_pos_x
      y_shape_range = (0...tetriminio.get_shape[0].length)
      y_pos = tetriminio.cell_pos_y

      x_shape_range.each do |x|
        y_shape_range.each do |y|
          # Test if item is active.
          if tetriminio.get_shape[x][y]
            # Nil if outside game field.
            if x+x_pos < 0 || @playing_field_cells[x+x_pos].nil? || @playing_field_cells[x+x_pos][y+y_pos].nil?
              yield nil
            else
              yield @playing_field_cells[x+x_pos][y+y_pos]
            end
          end
        end
      end
    end

  end
end