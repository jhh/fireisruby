# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'tetriminio'

module Core
  module Providers

    class TetriminioProvider

      # Returns all tetriminio shapes.
      def get_tetriminio_variants
        get_tetriminios
      end

      def get_tetriminio_by_name(name)
        result = get_tetriminio_variants.detect do |t|
          t.name == name
        end
        raise ArgumentError.new("There is no Tetrimino that correspond to \"%s\"" % name) unless result
        result
      end

      def reset
        @tetriminios = nil
      end

      private
      def get_tetriminios

        if @tetriminios
          return @tetriminios
        end

        # Tetriminio shaped like a "I".
        i = Tetriminio.new
        i.name = "I"
        # Array does not represent correct representation according to x and y coordinates, but it gets corrected when
        # applying transpose on array. This makes it much easier to visualize shape of tetriminio in code.
        i.shape_0 = [[false, true, false], [false, true, false], [false, true, false], [false, true, false]].transpose
        i.shape_90 = [[false, false, false, false], [false, false, false, false], [true, true, true, true]].transpose
        i.shape_180 = [[false, true, false], [false, true, false], [false, true, false], [false, true, false]].transpose
        i.shape_270 = [[false, false, false, false], [false, false, false, false], [true, true, true, true]].transpose

        # Tetriminio shaped like a "J".
        j = Tetriminio.new
        j.name = "J"
        j.shape_0 = [[false, true, false], [false, true, false], [true, true, false]].transpose
        j.shape_90 = [[true, false, false], [true, true, true]].transpose
        j.shape_180 = [[false, true, true], [false, true, false], [false, true, false]].transpose
        j.shape_270 = [[false, false, false], [true, true, true], [false, false, true]].transpose

        # Tetriminio shaped like a "L".
        l = Tetriminio.new
        l.name = "L"
        l.shape_0 = [[false, true, false], [false, true, false], [false, true, true]].transpose
        l.shape_90 = [[false, false, false], [true, true, true], [true, false, false]].transpose
        l.shape_180 = [[true, true, false], [false, true, false], [false, true, false]].transpose
        l.shape_270 = [[false, false, true], [true, true, true]].transpose

        # Tetriminio shaped like a "O".
        o = Tetriminio.new
        o.name = "O"
        o.shape_0 = [[true, true], [true, true]].transpose
        o.shape_90 = [[true, true], [true, true]].transpose
        o.shape_180 = [[true, true], [true, true]].transpose
        o.shape_270 = [[true, true], [true, true]].transpose

        # Tetriminio shaped like a "S".
        s = Tetriminio.new
        s.name = "S"
        s.shape_0 = [[false, true, true], [true, true, false]].transpose
        s.shape_90 = [[true, false], [true, true], [false, true]].transpose
        s.shape_180 = [[false, true, true], [true, true, false]].transpose
        s.shape_270 = [[true, false], [true, true], [false, true]].transpose

        # Tetriminio shaped like a "T".
        t = Tetriminio.new
        t.name = "T"
        t.shape_0 = [[false, false, false], [true, true, true], [false, true, false]].transpose
        t.shape_90 = [[false, true, false], [true, true, false], [false, true, false]].transpose
        t.shape_180 = [[false, true, false], [true, true, true], [false, false, false]].transpose
        t.shape_270 = [[false, true, false], [false, true, true], [false, true, false]].transpose

        # Tetriminio shaped like a "Z".
        z = Tetriminio.new
        z.name = "Z"
        z.shape_0 = [[true, true, false], [false, true, true]].transpose
        z.shape_90 = [[false, true], [true, true], [true, false]].transpose
        z.shape_180 = [[true, true, false], [false, true, true]].transpose
        z.shape_270 = [[false, true], [true, true], [true, false]].transpose

        @tetriminios = [i, j, l, o, s, t, z]
      end
    end
  end
end