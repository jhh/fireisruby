# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Core
  module Providers

    class Tetriminio
      attr_accessor :name
      attr_accessor :cell_pos_x
      attr_accessor :cell_pos_y
      attr_accessor :shape_0
      attr_accessor :shape_90
      attr_accessor :shape_180
      attr_accessor :shape_270

      def initialize
        @shapes = [:shape_0, :shape_90, :shape_180, :shape_270]
        set_shape # Initialize current_shape to a default value.
      end

      def get_shape_symbols
        @shapes.dup
      end

      def get_current_shape_symbol
        @current_shape_symbol
      end

      # Sets one of the predefined shapes defined in get_shapes method.
      # A tetriminio can be pointed at four angels, 0, 90, 180 and
      # 270 degree. Shapes allowed are therefor :shape_0-270.
      # Returns valid shape, a default if input is not valid.
      #
      def set_shape(shape = :shape_0)
        @current_shape_symbol = @shapes.include?(shape) ? shape : :shape_0
        send @current_shape_symbol
      end

      # Get _current_ shape of tetriminio.
      # Returns shape content.
      # @see set_shape.
      #
      def get_shape
        send @current_shape_symbol
      end

      # Update shape with new grade, e.g.
      # if current shape is 0 degree,
      # new degree is 90 degree "right".
      def rotate_tetriminio_90_degree_right
        case @current_shape_symbol
          when :shape_0
            @current_shape_symbol = :shape_90
          when :shape_90
            @current_shape_symbol = :shape_180
          when :shape_180
            @current_shape_symbol = :shape_270
          when :shape_270
            @current_shape_symbol = :shape_0
        end
        get_shape
      end

      # Update shape with new grade, e.g.
      # if current shape is 90 degree,
      # new degree is 0 degree "left".
      def rotate_tetriminio_90_degree_left
        case @current_shape_symbol
          when :shape_0
            @current_shape_symbol = :shape_270
          when :shape_90
            @current_shape_symbol = :shape_0
          when :shape_180
            @current_shape_symbol = :shape_90
          when :shape_270
            @current_shape_symbol = :shape_180
        end
        get_shape
      end

      def <=>(other)
        throw ArgumentError.new("Expected other to be a #{self.class.to_s}") unless other.is_a? self.class
        if get_shape && !other.get_shape
          1
        elsif !get_shape && other.get_shape
          -1
        elsif @name != other.name
          @name <=> other.name
        else
          _compare_two_2d_arrays(get_shape, other.get_shape)
        end
      end

      def _compare_two_2d_arrays(a, b)
        throw ArgumentError.new("Expected arrays parameters.") unless (a.is_a? Array) && (b.is_a? Array)
        a_flat_str = a.flatten(1).join
        b_flat_str = b.flatten(1).join
        a_flat_str <=> b_flat_str
      end

    end
  end
end