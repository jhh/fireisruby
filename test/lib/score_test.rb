# fireisruby
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
require 'test/unit'
require_relative '../../lib/score'
require 'tmpdir'

class ScoreTest < Test::Unit::TestCase

  def setup
    unless Score.respond_to? :override_score_list=
      class << Score
        def override_score_list=(obj)
          @score_list=obj
        end
        def override_score_file_full_path=(path)
          @score_file_full_path=path
        end
        def override_score_file_full_path
          @score_file_full_path
        end
      end
    end
    Score.override_score_list = []
    Score.override_score_file_full_path = File.join(Dir.tmpdir, '.FireIsRubyScore')
  end

  def teardown
    # Do nothing
  end

  def test_get_empty_score_list
    expected = []
    actual = Score.get_score_list
    assert_equal expected, actual, 'Empty score list found'
  end

  def test_score_list_is_sorted
    add_a_few_scores
    result = Score.get_score_list
    assert_equal 'High score', result[0][:name], 'High score user on top of list'
    assert_equal 'Low score', result[2][:name], 'Low score user on bottom of list'
    assert_equal 8, result[1][:score], 'Medium score of 8 points in middle of list'
  end

  def test_score_is_within_top_list
    add_ten_scores
    assert_equal true, Score.is_score_within_top_list?(11), 'Score of 11 is within top ten'
  end

  def test_score_is_not_within_top_list
    add_ten_scores
    assert_equal false, Score.is_score_within_top_list?(1), 'Score of 1 is not within top ten'
  end

  def test_add_score_to_full_list
    add_ten_scores
    expected_length = 10
    assert_equal expected_length, Score.get_score_list.length
    Score.register_score 'Foo', 12
    assert_equal expected_length, Score.get_score_list.length, 'Score list length remain same'
    expected_position = 4
    actual_position = Score.get_score_list.index { |item| item[:name] == 'Foo' } + 1
    assert_equal expected_position, actual_position, 'Added score found in expected position'
  end

  def test_get_max
    expected = 10
    actual = Score.get_max_size_of_score_list
    assert_equal expected, actual, 'Max length of score list available'
  end

  def test_yaml_score_list
    add_a_few_scores
    expected = Score.get_score_list
    actual = Score.undo_yaml_score_list(Score.yaml_score_list)
    assert_equal expected[0][:name], actual[0][:name], 'First item seems legit'
    assert_equal expected, actual, 'Successfully serialized and de-serialized score list'
  end

  def test_save_and_load_score_from_disk
    add_a_few_scores
    actual_pre_save = Score.get_score_list
    Score.save_score_list_to_disk
    Score.load_score_list_from_disk
    actual_post_save = Score.get_score_list
    assert_equal actual_pre_save, actual_post_save, 'Saving and loading from disk appears fine'
  end

  def test_load_score_from_disk_missing_file
    # Make sure test file does not exist before test.
    score_file = Score.override_score_file_full_path
    File.delete score_file if File.exist? score_file
    # Expect an operation system error, since file does not exist.
    assert_raise Errno::ENOENT do Score.load_score_list_from_disk end
    # We can pass a True parameter, to create score file before reading.
    assert_nothing_raised do Score.load_score_list_from_disk true end
  end

  def add_a_few_scores
    Score.register_score 'Medium score', 8
    Score.register_score 'Low score', 5
    Score.register_score 'High score', 12
  end

  def add_ten_scores
    5.upto(15) do |i|
      Score.register_score "Name #{i}", i
    end
  end
end