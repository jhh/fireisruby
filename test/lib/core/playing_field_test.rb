# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'test/unit'
require_relative '../../../lib/core'

class PlayingFieldTest < Test::Unit::TestCase

  def setup
    @pf = Core.get_new_playing_field
  end

  def teardown
    # Do nothing
  end

  def test_get_playing_field_height
    expected = 20
    actual = @pf.playing_field_height
    assert_equal expected, actual, 'Playing field is height %d' % expected
  end

  def test_get_playing_field_width
    expected = 10
    actual = @pf.playing_field_width
    assert_equal expected, actual, 'Playing field width is %d' % expected
  end

  def test_new_playing_field_does_not_consist_of_only_inactive_cells
    cells = @pf.playing_field_cells
    actual = []
    cells.each do |row|
      row.each do |cell|
        actual << cell if cell.is_active
      end
    end
    assert((actual.length > 0), 'Some cells are active')
  end

  def test_new_playing_field_have_one_and_exactly_one_active_tetriminio
    exactly_one = true
    tetriminio = nil
    cells = @pf.playing_field_cells
    cells.each do |row|
      row.each do |cell|
        if cell.is_active
          tetriminio ||= cell.tetriminio
          exactly_one = false unless tetriminio.equal? cell.tetriminio
        end
      end
    end
    assert exactly_one && tetriminio, 'New playing field have one active tetriminio'
  end

  def test_dry_put_existing_tetriminio_returns_false
    tetriminio = @pf.falling_tetriminio
    expected = false
    actual = @pf.dry_put_tetriminio tetriminio
    assert_equal(expected, actual, 'Active tetriminio detected, not safe')
  end

  def test_dry_put_tetriminio_in_empty_playing_field_returns_true
    tetriminio = @pf.falling_tetriminio
    # Make cells inactive..
    @pf.put_tetriminio tetriminio, false
    expected = true
    actual = @pf.dry_put_tetriminio tetriminio
    assert_equal(expected, actual, "It's safe to put tetriminio here")
  end

  def test_get_random_tetriminio_returns_a_tetriminio
    assert (@pf.get_random_tetriminio.instance_of? Core::Providers::Tetriminio), 'Random tetriminio returns a Tetriminio'
  end

  def test_get_random_tetriminio_returns_different_tetriminios
    max_loops = 1_000
    loop_counter = 0
    while true do
      loop_counter += 1
      assert false, 'No two random found' unless loop_counter < max_loops
      break if @pf.get_random_tetriminio.<=>(@pf.get_random_tetriminio) != 0
    end
    assert true, 'Two random and different tetriminios found'
  end

  def test_get_random_tetriminio_might_also_return_two_equal_tetriminios
    max_loops = 1_000
    loop_counter = 0
    while true do
      loop_counter += 1
      assert false, 'No two random equals found' unless loop_counter < max_loops
      break if @pf.get_random_tetriminio.<=>(@pf.get_random_tetriminio) == 0
    end
    assert true, 'Two random but equals tetriminios found'
  end

  def test_rotate_tetriminio_90_degree_right
    original_rotation = @pf.falling_tetriminio.get_current_shape_symbol
    @pf.rotate_tetriminio_90_degree_right
    new_rotation = @pf.falling_tetriminio.get_current_shape_symbol
    assert_not_equal original_rotation, new_rotation, 'Rotation has changed'
  end

  def test_rotate_tetriminio_90_degree_left
    original_rotation = @pf.falling_tetriminio.get_current_shape_symbol
    @pf.rotate_tetriminio_90_degree_left
    new_rotation = @pf.falling_tetriminio.get_current_shape_symbol
    assert_not_equal original_rotation, new_rotation, 'Rotation has changed'
  end

  def test_move_tetriminio_down
    original_y = @pf.falling_tetriminio.cell_pos_y
    @pf.move_tetriminio_down
    new_y = @pf.falling_tetriminio.cell_pos_y
    assert_not_equal original_y, new_y, 'Position has changed'
  end

  def test_move_tetriminio_to_bottom
    old = @pf.falling_tetriminio
    @pf.move_tetriminio_to_bottom
    new = @pf.falling_tetriminio
    assert_not_equal new, old, 'New tetriminio has been created'
    # It takes a bit more effort to find exact position, due to shape
    # of tetriminio, so we just test within a range on the y-axe.
    expected = ((@pf.playing_field_height - old.get_shape[0].length)...@pf.playing_field_height)
    actual = old.cell_pos_y
    assert expected === actual, 'Y position is where expected'
  end

  def test_if_cell_contain_the_falling_tetriminio_or_not
    x_start = 0
    x_end = @pf.falling_tetriminio.get_shape.length
    y_start = 0
    y_end = @pf.falling_tetriminio.get_shape[0].length
    number_assertions = 0
    expected_assertions = x_end * y_end
    (x_start...x_end).each do |x|
      (y_start...y_end).each do |y|
        cell_coord_x = @pf.falling_tetriminio.cell_pos_x + x
        cell_coord_y = @pf.falling_tetriminio.cell_pos_y + y
        cell = @pf.playing_field_cells[cell_coord_x][cell_coord_y]
        actual = @pf.cell_contain_the_falling_tetriminio cell
        if @pf.falling_tetriminio.get_shape[x][y]
          assert actual, 'Falling tetriminio exists in [%d][%d]' % [cell_coord_x, cell_coord_x]
        else
          assert !actual, 'Falling tetriminio does not exists in [%d][%d]' % [cell_coord_x, cell_coord_x]
        end
        number_assertions += 1
      end
    end
    assert_equal expected_assertions, number_assertions, 'Number of assertions as expected'
  end

  def test_move_tetriminio_left
    x_old, y_old = @pf.falling_tetriminio.cell_pos_x, @pf.falling_tetriminio.cell_pos_y
    @pf.move_tetriminio_left
    x_new, y_new = @pf.falling_tetriminio.cell_pos_x, @pf.falling_tetriminio.cell_pos_y
    assert_equal x_old-1, x_new, 'X coordinate has changed with -1'
    assert_equal y_old, y_new, 'Y coordinate remain unchanged'
  end

  def test_move_tetriminio_right
    x_old, y_old = @pf.falling_tetriminio.cell_pos_x, @pf.falling_tetriminio.cell_pos_y
    @pf.move_tetriminio_right
    x_new, y_new = @pf.falling_tetriminio.cell_pos_x, @pf.falling_tetriminio.cell_pos_y
    assert_equal x_old+1, x_new, 'X coordinate has changed with +1'
    assert_equal y_old, y_new, 'Y coordinate remain unchanged'
  end

  def test_try_to_remove_solid_rows
    # Initialize quick and dirty test field.
    some_tetriminio = @pf.get_random_tetriminio
    (0...@pf.playing_field_width).each do |x|
      cell = @pf.playing_field_cells[x][(@pf.playing_field_height-1)]
      cell.tetriminio = some_tetriminio
      cell.is_active = true
    end
    expected = 1
    actual = @pf.try_to_remove_solid_rows
    assert_equal expected, actual, "Removed one row"
  end

end
