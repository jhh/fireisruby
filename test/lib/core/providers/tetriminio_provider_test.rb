# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'test/unit'
require_relative '../../../../lib/core'

class TetriminioProviderTest < Test::Unit::TestCase

  def setup
    @tp = Core::Providers::TetriminioProvider.new
  end

  def test_provider_have_seven_tetrimino_variants
    expected = 7;
    actual = @tp.get_tetriminio_variants.length
    assert_equal(expected, actual, "There are %d tetriminio variants." % [expected])
  end

  def test_get_tetrimino_by_name
    expected = Core::Providers::Tetriminio.new
    expected.name = "I"
    expected.shape_0 = [[false, true, false], [false, true, false], [false, true, false], [false, true, false]].transpose
    expected.set_shape :shape_0
    actual = @tp.get_tetriminio_by_name expected.name
    actual.set_shape :shape_0
    assert(expected.<=>(actual) == 0, "Tetriminio recieved by name is same as expected.")
  end

  def test_all_tetriminios_have_shapes_for_all_degrees
    @tp.get_tetriminio_variants.each do |tetri|
      msg = "Tetriminio variant %s got %d degree shape."
      assert(!tetri.shape_0.empty?, msg % [tetri.name, 0])
      assert(!tetri.shape_90.empty?, msg % [tetri.name, 90])
      assert(!tetri.shape_180.empty?, msg % [tetri.name, 180])
      assert(!tetri.shape_270.empty?, msg % [tetri.name, 270])
    end
  end

  def test_reset_method_create_new_variant_object
    original_variants = @tp.get_tetriminio_variants
    @tp.reset
    new_variants = @tp.get_tetriminio_variants
    assert_not_equal original_variants, new_variants, "After reset, tetriminio variants are not same object."
  end

  def get_tetriminio_variants_returns_same_variants_object
    original_variants = @tp.get_tetriminio_variants
    new_variants = @tp.get_tetriminio_variants
    assert_same original_variants, new_variants, "Tetriminio variants are the same, when method for recieve is called twice."
  end

end
