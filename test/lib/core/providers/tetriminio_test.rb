# FireIsRuby - a Tetris like game.
# Copyright (C) 2014  Jan Henrik Hasselberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'test/unit'
require_relative '../../../../lib/core'

class TetriminioTest < Test::Unit::TestCase

  Subj = Core::Providers # Module shortcut.

  def setup
    @t, @shapes = _create_a_test_tetriminio
  end


  def teardown
    # Do nothing
  end

  def test_get_current_shape_symbol_returns_default_when_unexpected_data_entered
    @t.set_shape :foo
    actual = @t.get_current_shape_symbol
    expected = :shape_0
    assert_equal expected, actual, "Default value returned when :#{:foo} entered."
  end

  def test_get_shape_symbols_method_returns_four_known_shapes
    shapes_actual = @t.get_shape_symbols
    shapes_expected = [:shape_0, :shape_90, :shape_180, :shape_270]
    assert shapes_expected - shapes_actual == [], "Known set of shapes returned from shape method."
  end

  def test_set_shape_method_returns_four_known_shapes
    @t.get_shape_symbols.each do |shape|
      actual = @t.set_shape(shape)
      expected = @shapes.shift
      arr_compare = @t._compare_two_2d_arrays(expected, actual) # Do <=> like comparing.
      assert(arr_compare == 0, "Shape #{shape} returned from get_shape method as expected.")
    end
  end

  def test_current_shape_symbol_is_initialized_to_a_known_default_symbol_in_new_instance
    expected_default_symbol = :shape_0
    actual_default_symbol = Subj::Tetriminio.new.get_current_shape_symbol
    assert_equal expected_default_symbol, actual_default_symbol, "Current shape is initialized in new instance."
  end

  def test_rotate_tetriminio_right
    expected = :shape_0
    assert_equal expected, @t.get_current_shape_symbol, "Initial position :#{expected}"

    @t.rotate_tetriminio_90_degree_right
    expected = :shape_90
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_right
    expected = :shape_180
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_right
    expected = :shape_270
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_right
    expected = :shape_0
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."
  end

  def test_rotate_tetriminio_left
    expected = :shape_0
    assert_equal expected, @t.get_current_shape_symbol, "Initial position :#{expected}"

    @t.rotate_tetriminio_90_degree_left
    expected = :shape_270
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_left
    expected = :shape_180
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_left
    expected = :shape_90
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."

    @t.rotate_tetriminio_90_degree_left
    expected = :shape_0
    assert_equal expected, @t.get_current_shape_symbol, "Rotate tetriminio to #{expected}."
  end

  # Tests the <=> implementation.
  def test_tetriminio_comparer_is_equal
    same_as_t, = _create_a_test_tetriminio
    result = @t <=> same_as_t
    assert(result == 0, "<=> returns 0 for equal Tetriminio")
  end

  def test_tetriminio_comparer_is_not_equal
    # Compare names.
    different_name, = _create_a_test_tetriminio
    different_name.name = @t.name.next
    result = @t <=> different_name
    assert_equal result, -1, "<=> returns -1 for parameter Tetriminio with different name"

    # Compare structure.
    different_struct, = _create_a_test_tetriminio
    # Swap shape_0 and shape_90 values.
    different_struct.shape_0, different_struct.shape_90 = different_struct.shape_90, different_struct.shape_0
    result = @t <=> different_struct
    assert_equal result, 1, "<=> returns +1 for parameter Tetriminio with different structure"

    # One or the other is nil
    has_nil_shape, = _create_a_test_tetriminio
    has_nil_shape.shape_0=nil
    result = @t <=> has_nil_shape
    assert_equal 1, result, "<=> returns +1 for parameter Tetriminio with a nil structure"
    result = has_nil_shape <=> @t
    assert_equal -1, result, "<=> returns -1 for this Tetriminio with a nil structure"
  end

  def _create_a_test_tetriminio
    shapes = [[true, false], [false, true], [true, false], [false, true]]
    t = Subj::Tetriminio.new
    t.shape_0, t.shape_90, t.shape_180, t.shape_270 = *shapes
    t.name = "f" # flip-flop
    return t, shapes
  end
end
